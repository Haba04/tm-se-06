package ru.habibrahmanov.tm.command.project;

import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.command.AbstractCommand;

import java.util.List;
import java.util.Map;

public class ProjectFindAllCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-find-all";
    }

    @Override
    public String getDescription() {
        return "show all projects";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL PROJECTS]");
        List<Project> projectList = bootstrap.getProjectService().findAll(bootstrap.getUserService().getCurrentUser().getId());
        for (Project project : projectList) {
            System.out.println("PROJECT ID: " + project.getId() + " / NAME: " + project.getName());

        }
    }
}
