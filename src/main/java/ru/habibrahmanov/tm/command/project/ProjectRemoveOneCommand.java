package ru.habibrahmanov.tm.command.project;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class ProjectRemoveOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "remove project by id";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER ID PROJECT:");
        String projectId = bootstrap.getScanner().nextLine();
        bootstrap.getProjectService().remove(projectId);
        bootstrap.getTaskService().removeAll(projectId);
        System.out.println("PROJECT REMOVED SUCCESSFULLY");
    }
}
