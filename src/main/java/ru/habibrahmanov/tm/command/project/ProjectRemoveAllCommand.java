package ru.habibrahmanov.tm.command.project;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class ProjectRemoveAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove-all";
    }

    @Override
    public String getDescription() {
        return "remove all projects";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CLEAR]");
        bootstrap.getProjectService().removeAll();
        System.out.println("DELETE ALL PROJECTS");
    }
}
