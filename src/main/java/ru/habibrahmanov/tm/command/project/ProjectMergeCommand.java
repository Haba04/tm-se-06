package ru.habibrahmanov.tm.command.project;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class ProjectMergeCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-merge";
    }

    @Override
    public String getDescription() {
        return "update if project is already exist, else create new project";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT MERGE]");
        System.out.println("ENTER PROJECT ID");
        String projectId = bootstrap.getScanner().nextLine();
        System.out.println("ENTER PROJECT NAME");
        String name = bootstrap.getScanner().nextLine();
//        bootstrap.getProjectService().merge(projectId, name);
        System.out.println("PROJECT MERGE SUCCESSFULLY");
    }
}
