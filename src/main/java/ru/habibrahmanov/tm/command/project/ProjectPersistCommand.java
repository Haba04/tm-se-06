package ru.habibrahmanov.tm.command.project;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class ProjectPersistCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-persist";
    }

    @Override
    public String getDescription() {
        return "create new project.";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        String name = bootstrap.getScanner().nextLine();
        bootstrap.getProjectService().persist(name, bootstrap.getUserService().getCurrentUser().getId());
        System.out.println("CREATE NEW PROJECT SUCCESSFULLY");
    }
}
