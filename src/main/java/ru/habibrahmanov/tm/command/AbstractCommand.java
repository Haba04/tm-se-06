package ru.habibrahmanov.tm.command;

import ru.habibrahmanov.tm.bootstrap.Bootstrap;

public abstract class AbstractCommand {
        protected Bootstrap bootstrap;
        public void setBootstrap(Bootstrap bootstrap) {
            this.bootstrap = bootstrap;
        }
        public abstract String getName();
        public abstract String getDescription();
        public abstract boolean secure();
        public abstract void execute() throws Exception;
}

