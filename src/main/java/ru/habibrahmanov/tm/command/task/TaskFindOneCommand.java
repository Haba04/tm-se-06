package ru.habibrahmanov.tm.command.task;

import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Task;

public class TaskFindOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-find";
    }

    @Override
    public String getDescription() {
        return "find task by id";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[Find task by id]");
        System.out.println("[Enter task by id]");
        String taskId = bootstrap.getScanner().nextLine();
        Task task = bootstrap.getTaskService().findOne(taskId, bootstrap.getUserService().getCurrentUser().getId());
        System.out.println("TASK ID: " + task.getId() + " / NAME: " + task.getName());
    }
}
