package ru.habibrahmanov.tm.command.task;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class TaskFindAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-find-all";
    }

    @Override
    public String getDescription() {
        return "find all tasks";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW ALL TASKS]");
//        for (int i = 0; i < bootstrap.getTaskService().findAll().size(); i++) {
//            System.out.println("ID: " + bootstrap.getTaskService().findAll().get(i).getId() + " / NAME: " + bootstrap.getTaskService().findAll().get(i).getName());
//        }
        bootstrap.getTaskService().findAll(bootstrap.getUserService().getCurrentUser().getId()).forEach((v)-> System.out.println("TASK ID: " + v.getId() + " / NAME: " + v.getName()));
    }
}
