package ru.habibrahmanov.tm.command.task;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class TaskMergeCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-merge";
    }

    @Override
    public String getDescription() {
        return "update if task is already exist, else create new task";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK MERGE]");
        System.out.println("ENTER PROJECT ID:");
        String projectId = bootstrap.getScanner().nextLine();
        System.out.println("ENTER TASK ID:");
        String taskId = bootstrap.getScanner().nextLine();
        System.out.println("ENTER TASK NAME:");
        String name = bootstrap.getScanner().nextLine();
        String userId = bootstrap.getUserService().getCurrentUser().getId();
        bootstrap.getTaskService().merge(projectId, taskId, name, userId);
        System.out.println("TASK MERGE SUCCESSFULLY");
    }
}
