package ru.habibrahmanov.tm.command.task;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class TaskRemoveOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "remove all tasks";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER TASK ID:");
        String taskId = bootstrap.getScanner().nextLine();
        bootstrap.getTaskService().remove(taskId);
        System.out.println("TASK REMOVED SUCCESSFULLY");
    }
}
