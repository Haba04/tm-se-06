package ru.habibrahmanov.tm.command.task;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-update";
    }

    @Override
    public String getDescription() {
        return "update task by id";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK UPDATE]");
        System.out.println("ENTER TASK ID:");
        String taskId = bootstrap.getScanner().nextLine();
        System.out.println("ENTER TASK NAME:");
        String name = bootstrap.getScanner().nextLine();
        bootstrap.getTaskService().update(bootstrap.getUserService().getCurrentUser().getId(), taskId, name);
        System.out.println("TASK UPDATED SUCCESSFULLY");
    }
}
