package ru.habibrahmanov.tm.command.task;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class TaskPersistCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-persist";
    }

    @Override
    public String getDescription() {
        return "create new task";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER PROJECT ID");
        String projectId = bootstrap.getScanner().nextLine();
        System.out.println("ENTER TASK NAME");
        String name = bootstrap.getScanner().nextLine();
        String userId = bootstrap.getUserService().getCurrentUser().getId();
        bootstrap.getTaskService().persist(projectId, userId, name);
        System.out.println("TASK CREATE SUCCESSFULLY");
    }
}
