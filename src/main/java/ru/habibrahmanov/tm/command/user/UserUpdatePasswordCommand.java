package ru.habibrahmanov.tm.command.user;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class UserUpdatePasswordCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "up";
    }

    @Override
    public String getDescription() {
        return "user password update";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PASSWORD]");
        System.out.println("ENTER CURRENT PASSWORD:");
        String currentPassword = bootstrap.getScanner().nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        String newPassword = bootstrap.getScanner().nextLine();
        System.out.println("ENTER NEW PASSWORD AGAIN:");
        String newPasswordConfirm = bootstrap.getScanner().nextLine();
        bootstrap.getUserService().updatePassword(bootstrap.getUserService().getCurrentUser(), currentPassword, newPassword, newPasswordConfirm);
        System.out.println("UPDATE PASSWORD SUCCESSFULLY");
    }

}
