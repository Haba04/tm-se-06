package ru.habibrahmanov.tm.command.user;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class UserLogoutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public String getDescription() {
        return "you are logged out";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getUserService().logout();
        System.out.println("YOU ARE LOGGED OUT");
    }
}
