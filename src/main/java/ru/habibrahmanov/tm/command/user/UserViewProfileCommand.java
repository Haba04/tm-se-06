package ru.habibrahmanov.tm.command.user;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class UserViewProfileCommand extends AbstractCommand {
    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[VIEW PROFILE]");
        bootstrap.getUserService().viewProfile();
        System.out.println("your login: " + bootstrap.getUserService().viewProfile().getLogin());
        System.out.println("your password: " + bootstrap.getUserService().viewProfile().getPassword());
    }
}
