package ru.habibrahmanov.tm.command.user;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class UserRegistryCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-registry";
    }

    @Override
    public String getDescription() {
        return "new user registration";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE NEW USER]");
        System.out.println("ENTER LOGIN:");
        String login = bootstrap.getScanner().nextLine();
        System.out.println("ENTER PASSWORD:");
        String password = bootstrap.getScanner().nextLine();
        System.out.println("ENTER PASSWORD AGAIN:");
        String passwordConfirm = bootstrap.getScanner().nextLine();
        bootstrap.getUserService().registryUser(login, password, passwordConfirm);
        System.out.println("CREATE NEW USER SUCCESSFULLY");
    }
}
