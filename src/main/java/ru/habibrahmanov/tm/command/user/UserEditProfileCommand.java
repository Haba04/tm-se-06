package ru.habibrahmanov.tm.command.user;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class UserEditProfileCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "edit-profile";
    }

    @Override
    public String getDescription() {
        return "user edit profile";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("EDIT PROFILE");
        System.out.println("ENTER NEW LOGIN");
        String login = bootstrap.getScanner().nextLine();
        bootstrap.getUserService().editProfile(login);
    }
}
