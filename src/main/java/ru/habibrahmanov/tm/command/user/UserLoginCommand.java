package ru.habibrahmanov.tm.command.user;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class UserLoginCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "login";
    }

    @Override
    public String getDescription() {
        return "user authorization in the system";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[AUTHORIZATION]");
        System.out.println("ENTER LOGIN:");
        String login = bootstrap.getScanner().nextLine();
        System.out.println("ENTER PASSWORD:");
        String password = bootstrap.getScanner().nextLine();
        bootstrap.getUserService().login(login, password);
        System.out.println("YOU ARE LOGGED IN");
    }
}
