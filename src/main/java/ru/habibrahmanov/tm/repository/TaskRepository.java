package ru.habibrahmanov.tm.repository;

import ru.habibrahmanov.tm.entity.Task;

import java.util.*;

public class TaskRepository {
    private Map<String, Task> taskMap = new LinkedHashMap<>();

    public void persist(Task task) {
        taskMap.put(task.getId(), task);
    }

    public Task findOne(String taskId, String userId) {
        for (Task task : taskMap.values()) {
            if (task.getUserId().equals(userId)) {
                taskMap.get(taskId);
            }
        }
        return null;
    }

    public List<Task> findAll(String userId) {
        List<Task> taskList = new ArrayList<>();
        for (Task task : taskMap.values()){
            if (task.getUserId().equals(userId))
            taskList.add(task);
        }
        return taskList;
    }

    public void remove(String taskId) {
        taskMap.remove(taskId);
    }

    public boolean removeAll(String projectId) {
        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entryTask = iterator.next();
            if (entryTask.getValue().getProjectId().equals(projectId)) {
                iterator.remove();
            }
        }
        return false;
    }

    public void update(String userId, String taskId, String name) {
        if (taskMap.get(taskId).getUserId().equals(userId)) {
            taskMap.get(taskId).setName(name);
        }
    }

    public void merge(Task task) {
        if (taskMap.containsKey(task.getId())) {
            update(task.getUserId(), task.getId(), task.getName());
        } else {
            persist(task);
        }
    }
}

