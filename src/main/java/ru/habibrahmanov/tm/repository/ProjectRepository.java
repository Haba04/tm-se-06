package ru.habibrahmanov.tm.repository;

import ru.habibrahmanov.tm.entity.Project;

import java.util.*;


public class ProjectRepository {
    private Map<String, Project> projectMap = new LinkedHashMap<>();

    public Map<String, Project> getProjectMap() {
        return projectMap;
    }

    public void persist(Project project) {
        projectMap.put(project.getId(), project);
    }

    public List<Project> findAll(String userId) {
        List<Project> projectList = new ArrayList<>();
        for (Project project : projectMap.values()) {
            if (project.getUserId().equals(userId)) {
                projectList.add(project);
            }
        }
        return projectList;
    }

    public Project findOne(String projectId) {
        return projectMap.get(projectId);
    }

    public void removeAll() {
        projectMap.clear();
    }

    public void remove(String projectId) {
        projectMap.remove(projectId);
    }

    // if there is a project, edit it, if not, create
    public void merge(Project project) {
        if (projectMap.containsKey(project.getId())) {
            update(project.getUserId(), project.getId(), project.getName());
        }
        persist(project);
    }

    public void update(String userId, String projectId, String name) {
        if (projectMap.get(projectId).getUserId().equals(userId)) {
            projectMap.get(projectId).setName(name);
        }
    }
}

