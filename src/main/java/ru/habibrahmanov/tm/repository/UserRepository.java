package ru.habibrahmanov.tm.repository;

import ru.habibrahmanov.tm.entity.User;

import java.util.*;

public class UserRepository {
    private Map<String, User> userMap = new LinkedHashMap<>();

    public void persist(User user) {
        userMap.put(user.getId(), user);
    }

    public User findOne(final String userId) {
        return userMap.get(userId);
    }

    public User findByLogin(final String login) {
        User user = null;
        List<User> userList = new ArrayList<>(userMap.values());
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getLogin().equals(login))
                user =  userList.get(i);
        }
        return user;
    }

    public List<User> findAll() {
        List<User> userList = new ArrayList<>(userMap.values());
        return userList;
    }

    public void removeOne(final String login) {
        userMap.remove(login);
    }

    public void removeAll() {
        userMap.clear();
    }

    public void update(String userId, String login) {
        userMap.get(userId).setLogin(login);
    }
}
