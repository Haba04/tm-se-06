package ru.habibrahmanov.tm;

import ru.habibrahmanov.tm.bootstrap.Bootstrap;
import ru.habibrahmanov.tm.command.system.HelpCommand;
import ru.habibrahmanov.tm.command.project.*;
import ru.habibrahmanov.tm.command.task.*;
import ru.habibrahmanov.tm.command.user.*;

public class Application {
    public static final Class[] COMMANDS = {
            HelpCommand.class,
            ProjectFindAllCommand.class, ProjectMergeCommand.class, ProjectPersistCommand.class, ProjectRemoveAllCommand.class,
            ProjectRemoveOneCommand.class, ProjectUpdateCommand.class,
            TaskFindAllCommand.class, TaskFindOneCommand.class, TaskMergeCommand.class, TaskPersistCommand.class,
            TaskRemoveOneCommand.class, TaskUpdateCommand.class,
            UserLoginCommand.class, UserEditProfileCommand.class, UserEditProfileCommand.class, UserRegistryCommand.class,
            UserUpdatePasswordCommand.class, UserViewProfileCommand.class, AdminRegistryCommand.class, UserLogoutCommand.class
    };

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(COMMANDS);
        bootstrap.start();
    }
}
