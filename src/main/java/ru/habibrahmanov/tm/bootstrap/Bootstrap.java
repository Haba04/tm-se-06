package ru.habibrahmanov.tm.bootstrap;

import ru.habibrahmanov.tm.repository.ProjectRepository;
import ru.habibrahmanov.tm.repository.TaskRepository;
import ru.habibrahmanov.tm.repository.UserRepository;
import ru.habibrahmanov.tm.service.ProjectService;
import ru.habibrahmanov.tm.service.TaskService;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.service.UserService;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public final class Bootstrap {
    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskService taskService = new TaskService(taskRepository);
    private UserRepository userRepository = new UserRepository();
    private UserService userService = new UserService(userRepository, taskService);
    private Scanner scanner = new Scanner(System.in);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init(final Class... commandsClass) {
        try {
            if (commandsClass == null) return;
            for (Class clazz : commandsClass) {
                registry(clazz);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registry(Class clazz) throws IllegalAccessException, InstantiationException {
        AbstractCommand abstractCommand = (AbstractCommand) clazz.newInstance();
        abstractCommand.setBootstrap(this);
        final String nameCommand = abstractCommand.getName();
        final String descriptionCommand = abstractCommand.getDescription();
        if (nameCommand == null || nameCommand.isEmpty()) return;
        if (descriptionCommand == null || descriptionCommand.isEmpty()) return;
        commands.put(nameCommand, abstractCommand);
    }

    public void start() throws Exception {
        System.out.println("***WELCOME TO TASK MANAGER***");
        System.out.println("Enter \"help\" to show all commands.");
        String command = "";
        while (!command.equals("exit")) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        if (abstractCommand.secure() || (!abstractCommand.secure() && userService.isAuth())) {
            abstractCommand.execute();
        }
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public UserService getUserService() {
        return userService;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }
}
