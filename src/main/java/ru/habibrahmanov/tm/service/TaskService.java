package ru.habibrahmanov.tm.service;

import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.repository.TaskRepository;
import java.util.List;
import java.util.UUID;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void persist(String projectId, String userId, String name) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty()) {
            throw new IncorrectValueException();
        }
        taskRepository.persist(new Task(name, UUID.randomUUID().toString(), projectId, userId));
    }

    public Task findOne(String taskId, String userId) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty()) {
            throw new IncorrectValueException();
        }
        return taskRepository.findOne(taskId, userId);
    }

    public List<Task> findAll(String userId) throws IncorrectValueException {
        if (taskRepository.findAll(userId).isEmpty()) {
            throw new IncorrectValueException("LIST IS EMPTY");
        }
        return taskRepository.findAll(userId);
    }

    public void remove(String taskId) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty()) {
            throw new IncorrectValueException();
        }
        taskRepository.remove(taskId);
    }

    public void removeAll(String projectId) {
        taskRepository.removeAll(projectId);
    }

    public void update(String userId, String taskId, String name) throws IncorrectValueException {
        if (userId == null || userId.isEmpty() || taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        taskRepository.update(userId, taskId, name);
    }

    public void merge(String projectId, String taskId, String name, String userId) throws IncorrectValueException {
        if (taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        taskRepository.merge(new Task(name, taskId, projectId, userId));
    }
}
