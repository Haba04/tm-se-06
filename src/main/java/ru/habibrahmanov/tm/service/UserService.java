package ru.habibrahmanov.tm.service;

import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.enumeration.Role;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.repository.UserRepository;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class UserService {
    private UserRepository userRepository;
    private TaskService taskService;
    private User currentUser = null;

    public UserService(UserRepository userRepository, TaskService taskService) {
        this.userRepository = userRepository;
        this.taskService = taskService;
    }

    public void registryAdmin(String login, String password, String passwordConfirm) throws IllegalArgumentException, UnsupportedEncodingException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (userRepository.findByLogin(login) != null) throw new IllegalArgumentException("SUCH USER EXISTS");
        if (!password.equals(passwordConfirm)) throw new IllegalArgumentException("PASSWORDS DO NOT MATCH");
        userRepository.persist(new User(login, createPasswordHashMD5(password), Role.ADMIN));
    }

    public void registryUser(String login, String password, String passwordConfirm) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (userRepository.findByLogin(login) != null) throw new IllegalArgumentException("SUCH USER EXISTS");
        if (!password.equals(passwordConfirm)) throw new IllegalArgumentException("PASSWORDS DO NOT MATCH");
        userRepository.persist(new User(login, createPasswordHashMD5(password), Role.USER));
    }

    public void updatePassword(final User currentUser, final String curPassword, final String newPassword, final String newPasswordConfirm) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if (curPassword == null || curPassword.isEmpty()) return;
        if (newPassword == null || newPassword.isEmpty()) return;
        if (newPasswordConfirm == null || newPasswordConfirm.isEmpty()) return;
        if (!currentUser.getPassword().equals(createPasswordHashMD5(curPassword))) {
            throw new IllegalArgumentException("CURRENT PASSWORD DOES NOT MATCH USER PASSWORD: " + currentUser.getLogin());
        }
        if (!newPassword.equals(newPasswordConfirm)) throw new IllegalArgumentException("PASSWORDS DO NOT MATCH");
        currentUser.setPassword(createPasswordHashMD5(newPassword));
    }

    public User login(final String login, final String password) throws IncorrectValueException, UnsupportedEncodingException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) throw new IncorrectValueException();
        if (password == null || password.isEmpty()) throw new IncorrectValueException();
        final List<User> userList = userRepository.findAll();
        if (userList == null || userList.isEmpty()) throw new IncorrectValueException("NO USER CREATED");
        if (userRepository.findByLogin(login) == null) throw new IncorrectValueException("USER WITH SUCH LOGIN DOES NOT EXIST");
        final String currentPassword = userRepository.findByLogin(login).getPassword();
        final String passwordMD5 = createPasswordHashMD5(password);
        if (!currentPassword.equals(passwordMD5)) throw new IncorrectValueException("WRONG PASSWORD");
        currentUser = userRepository.findByLogin(login);
        return currentUser;
    }


    public User viewProfile() {
        return currentUser;
    }

    public void editProfile(String newLogin) throws IncorrectValueException {
        if (newLogin == null || newLogin.isEmpty()) throw new IncorrectValueException();
        currentUser.setLogin(newLogin);
    }

    public void logout() {
        currentUser = null;
    }

    public boolean isAuth() {
        return currentUser != null;
    }

    public String createPasswordHashMD5(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final byte[] bytePassword = password.getBytes("UTF-8");
        String md5Password = new String(MessageDigest.getInstance("MD5").digest(bytePassword));
        return md5Password;
    }

    public User getCurrentUser() {
        return currentUser;
    }
}
