package ru.habibrahmanov.tm.service;

import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.repository.ProjectRepository;

import java.util.List;
import java.util.UUID;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void persist(String projectName, String userId) throws IncorrectValueException {
        if (projectName == null || projectName.isEmpty()){
            throw new IncorrectValueException();
        }
        projectRepository.persist(new Project(projectName, UUID.randomUUID().toString(), userId));
    }

    public List<Project> findAll(String userId) throws IncorrectValueException {
        if (projectRepository.findAll(userId).isEmpty()) {
            throw new IncorrectValueException("PROJECT LIST EMPTY");
        }
        return projectRepository.findAll(userId);
    }

    public void removeAll() throws IncorrectValueException {
        if (projectRepository.getProjectMap().isEmpty()) {
            throw new IncorrectValueException("PROJECT LIST EMPTY");
        }
        projectRepository.removeAll();
    }

    public void remove(String projectId) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty()) {
            throw new IncorrectValueException();
        }
        projectRepository.remove(projectId);
    }

    public void update(String userId, String projectId, String name) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        projectRepository.update(userId, projectId, name);
    }

    public void merge(String projectId, String name, String userId) throws IncorrectValueException {
        if (projectId == null || projectId.isEmpty() || name == null || name.isEmpty()) {
            throw new IncorrectValueException();
        }
        projectRepository.merge(new Project(name, projectId, userId));
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }
}
